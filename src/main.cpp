/*
 *  main.cpp
 *
 *  Copyright (c) 2001-2019 Nick Dowell
 *
 *  This file is part of amsynth.
 *
 *  amsynth is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  amsynth is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with amsynth.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "main.h"

#if HAVE_CONFIG_H
#include "config.h"
#endif

#include "AudioOutput.h"
#include "Configuration.h"
#include "drivers/ALSAMidiDriver.h"
#include "drivers/OSSMidiDriver.h"
#ifdef WITH_GUI
#include "GUI/gui_main.h"
#include "GUI/MainWindow.h"
#endif
#include "JackOutput.h"
#include "lash.h"
#include "midi.h"
#include "MidiController.h"
#include "Synthesizer.h"
#include "VoiceAllocationUnit.h"
#include "VoiceBoard/LowPassFilter.h"

#ifdef WITH_NSM
#include "nsm/NsmClient.h"
#include "nsm/NsmHandler.h"
#endif

#if __APPLE__
#include "drivers/CoreAudio.h"
#endif

#include <thread>
#include <iostream>
#include <fcntl.h>
#include <fstream>
#include <getopt.h>
#include <unistd.h>
#include <string>
#include <climits>
#include <cstring>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <algorithm>

#include "gettext.h"
#define _(string) gettext (string)

#include <gtk/gtk.h>
#include <map>
#include <SDL2/SDL.h>
#include <SDL2/SDL_joystick.h>
#include <SDL2/SDL_hints.h>

static int gamepad1Buttons[12] = {0,0,0,0, 0,0,0,0, 0,0,0,0};
static int gamepad2Buttons[12] = {0,0,0,0, 0,0,0,0, 0,0,0,0};
static int gamepad3Buttons[12] = {0,0,0,0, 0,0,0,0, 0,0,0,0};
static int gamepad_offset = 0;
static bool use_pipe = false;
static std::vector<std::string> __background_text__;

/* mplayer command options
 mplayer -input cmdlist
radio_step_channel   Integer
radio_set_channel    String
radio_set_freq       Float
radio_step_freq      Float
seek                 Float [Integer]
edl_loadfile         String
edl_mark            
audio_delay          Float [Integer]
speed_incr           Float
speed_mult           Float
speed_set            Float
quit                 [Integer]
stop                
pause               
frame_step          
pt_step              Integer [Integer]
pt_up_step           Integer [Integer]
alt_src_step         Integer
loop                 Integer [Integer]
sub_delay            Float [Integer]
sub_step             Integer [Integer]
osd                  [Integer]
osd_show_text        String [Integer] [Integer]
osd_show_property_te String [Integer] [Integer]
osd_show_progression
volume               Float [Integer]
balance              Float [Integer]
use_master          
mute                 [Integer]
contrast             Integer [Integer]
gamma                Integer [Integer]
brightness           Integer [Integer]
hue                  Integer [Integer]
saturation           Integer [Integer]
frame_drop           [Integer]
sub_pos              Integer [Integer]
sub_alignment        [Integer]
sub_visibility       [Integer]
sub_load             String
sub_remove           [Integer]
vobsub_lang          [Integer]
sub_select           [Integer]
sub_source           [Integer]
sub_vob              [Integer]
sub_demux            [Integer]
sub_file             [Integer]
sub_log             
sub_scale            Float [Integer]
ass_use_margins      [Integer]
get_percent_pos     
get_time_pos        
get_time_length     
get_file_name       
get_video_codec     
get_video_bitrate   
get_video_resolution
get_audio_codec     
get_audio_bitrate   
get_audio_samples   
get_meta_title      
get_meta_artist     
get_meta_album      
get_meta_year       
get_meta_comment    
get_meta_track      
get_meta_genre      
switch_audio         [Integer]
switch_angle         [Integer]
switch_title         [Integer]
tv_start_scan       
tv_step_channel      Integer
tv_step_norm        
tv_step_chanlist    
tv_set_channel       String
tv_last_channel     
tv_set_freq          Float
tv_step_freq         Float
tv_set_norm          String
tv_set_brightness    Integer [Integer]
tv_set_contrast      Integer [Integer]
tv_set_hue           Integer [Integer]
tv_set_saturation    Integer [Integer]
forced_subs_only     [Integer]
dvb_set_channel      Integer Integer
switch_ratio         [Float]
vo_fullscreen        [Integer]
vo_ontop             [Integer]
file_filter          Integer
vo_rootwin           [Integer]
vo_border            [Integer]
screenshot           [Integer]
panscan              Float [Integer]
switch_vsync         [Integer]
loadfile             String [Integer]
loadlist             String [Integer]
run                  String
capturing           
change_rectangle     Integer Integer
teletext_add_dec     String
teletext_go_link     Integer
overlay_add          String Integer Integer Integer Integer
overlay_remove       Integer
dvdnav               String
menu                 String
set_menu             String [String]
help                
exit                
hide                 [Integer]
get_vo_fullscreen   
get_sub_visibility  
key_down_event       Integer
set_property         String String
get_property         String
step_property        String [Float] [Integer]
seek_chapter         Integer [Integer]
set_mouse_pos        Integer Integer
af_switch            String
af_add               String
af_del               String
af_clr              
af_cmdline           String String
gui                  String
*/

#include "subprocess.hpp"
#include <random>

static std::mt19937 *__rand_engine = NULL;

// defined in editor_pane.cpp //
void amsynth_glitch_background_redraw(int x, int y, bool drawbg, std::vector<std::string> txt);

static int AmsynthXID = 0;
static subprocess::Popen *mplayer_subproc = NULL;
static subprocess::Popen *mplayer_webcam_subproc = NULL;
static subprocess::Popen *mplayer_webcam_ascii_subproc = NULL;
static char mplayer_ascii_buf[2048];
//static char ascii_audio_keyboard[128];
static std::string desktop_type = "";
static std::thread ascii_audio_thread;

static std::vector<std::pair<std::string,int>>  ascii_audio_mapping = {
	{"Oscillator1Waveform",512},
	{"FilterResonance",513},
	{"FilterEnvAmount",514},
	{"FilterCutoff",515},

	{"Oscillator2Detune", 516},
	{"Oscillator2Waveform",517},

	{"LFOFreq",518},
	{"LFOWaveform",519},
	
	{"Oscillator2Octave",520},
	{"OscillatorMix", 521},
	
	{"LFOToOscillators", 522},
	{"LFOToFilterCutoff",523},
	{"LFOToAmp", 524},
	
	{"OscillatorMixRingMod",525},
	
	{"Oscillator1Pulsewidth", 526},
	{"Oscillator2Pulsewidth",527},
	
	{"ReverbRoomsize", 528},
	{"ReverbDamp", 529},
	{"ReverbWet",530},
	{"ReverbWidth", 531},
	
	{"AmpDistortion", 532},
	{"Oscillator2Sync",533},

	{"PortamentoTime",534},
	
	{"Oscillator2Pitch", 535},
	{"FilterType", 536},
	{"FilterSlope",537},

	{"LFOOscillatorSelect", 538},


};

static std::string string_replace(std::string input, std::string tag, std::string target) {
	size_t pos = 0;
	std::string str = input;
	while((pos = str.find(tag, pos)) != std::string::npos) {
		str.replace(pos, tag.length(), target);
		pos += target.length();
	}
	return str;
}

static std::vector<std::string> string_split(std::string text, std::string delims) {
	auto tokens = std::vector<std::string>();
	auto len = delims.size();
	std::size_t start = text.find(delims), end = 0;
	if (start == std::string::npos) {
		tokens.push_back(text);
		return tokens;
	}
	if(start != std::string::npos) tokens.push_back(text.substr(0, start));
	while((end = text.find(delims, start+len)) != std::string::npos) {
		tokens.push_back(text.substr(start+len, (end-len) - start));
		start = text.find(delims, end);
	}
	if(start != std::string::npos) tokens.push_back(text.substr(start+len));
	return tokens;
}


void ascii_audio_thread_func() {
	while (mplayer_webcam_ascii_subproc) {
		auto fp = mplayer_webcam_ascii_subproc->output();
		int fd = fileno(fp);
		int read_bytes = read(fd, mplayer_ascii_buf, sizeof(mplayer_ascii_buf));
		if (read_bytes > 0) {
			std::string frame = std::string(mplayer_ascii_buf, read_bytes);
			//std::cout << read_bytes << std::endl;
			// this is threadsafe because std::string copies all the bytes??? //
			__background_text__ = string_split(frame, "\n");
		}
	}
}

void mplayer_webcam(std::string mode) {
	//mplayer tv:// -tv driver=v4l2:width=320:height=240:fps=24:outfmt=mjpg -brightness 100 -contrast 50 -vo matrixview
	// note: most window managers draw the desktop and no longer support using window id 0
	bool draw_on_desktop = desktop_type == "fvwm";

	if (mode=="ascii") {
		if (not mplayer_webcam_ascii_subproc) {
			mplayer_webcam_ascii_subproc = new subprocess::Popen(
				{"mplayer", "tv://", "-tv", "driver=v4l2", "-vo", "aa:driver=stdout:width=40:height=40", "-really-quiet"},
				subprocess::input{subprocess::PIPE},
				subprocess::output{subprocess::PIPE}
			);
			ascii_audio_thread = std::thread(ascii_audio_thread_func);
		}
	}
	else if (not mplayer_webcam_subproc) {
		if (mode=="matrixview") {
			if (draw_on_desktop) {
				mplayer_webcam_subproc = new subprocess::Popen(
					{"mplayer", "-wid", "0", "tv://", "-tv", "driver=v4l2:width=320:height=240", "-brightness", "100", "-contrast", "75", "-vo", "matrixview", "-really-quiet"},
					subprocess::input{subprocess::PIPE}
				);
			}
			else {
				mplayer_webcam_subproc = new subprocess::Popen({"mplayer", "-wid", std::to_string(AmsynthXID).c_str(), "tv://", "-tv", "driver=v4l2:width=320:height=240", "-brightness", "100", "-contrast", "75", "-vo", "matrixview", "-really-quiet"});
			}
		}
		else if (mode=="ascii-debug") {
			mplayer_webcam_subproc = new subprocess::Popen(
				{"xterm", "-fn", "5x7", "-geometry", "160x120", "-e", "mplayer", "tv://", "-tv", "driver=v4l2", "-vo", "aa:driver=curses", "-really-quiet"},
				subprocess::input{subprocess::PIPE}
			);
		}
		else {
			if (draw_on_desktop) {
				mplayer_webcam_subproc = new subprocess::Popen(
					{"mplayer", "-wid", "0", "tv://", "-tv", "driver=v4l2", "-brightness", "10", "-contrast", "100", "-really-quiet"},
					subprocess::input{subprocess::PIPE}
				);
			}
			else {
				mplayer_webcam_subproc = new subprocess::Popen(
					{"mplayer", "-wid", std::to_string(AmsynthXID).c_str(), "tv://", "-tv", "driver=v4l2:width=320:height=240", "-brightness", "10", "-contrast", "100", "-quiet"},
					subprocess::input{subprocess::PIPE}
				);
			}
		}
	}
}

/* notes:
 -hardframedrop also glitches the audio, but can also cause the audio playback to fail entirely, TODO this should be an option
 without any frame dropping audio stutters very hard when video is played with the webcam,
 actually, looks like printing debug info to the console is the main cause of the glitching, so -quiet is required.
 {"mplayer", "-vo", "matrixview", "-x", "1280", "-idle", "-loop", "10", "-nojoystick", "-nomouseinput", "-slave", "-wid", std::to_string(AmsynthXID).c_str(), fname.c_str(), "-hardframedrop"}, 
 {"mplayer", "-vo", "matrixview", "-x", "1280", "-idle", "-loop", "10", "-nojoystick", "-nomouseinput", "-slave", "-wid", std::to_string(AmsynthXID).c_str(), fname.c_str()}, 

*/

void mplayer_play(std::string fname) {

	if (not mplayer_subproc) {
		fname = string_replace(fname, "%20", " ");  // some file managers replace spaces with %20
		fname = string_replace(fname, "%5B", "[");
		fname = string_replace(fname, "%5D", "]");
		//fname = string_replace(fname, "(", "\\(");
		//fname = string_replace(fname, ")", "\\)");
		if (AmsynthXID) {
			mplayer_subproc = new subprocess::Popen(
				{"mplayer", "-vo", "matrixview", "-x", "1280", "-idle", "-loop", "10", "-nojoystick", "-nomouseinput", "-slave", "-wid", std::to_string(AmsynthXID).c_str(), fname.c_str(), "-framedrop", "-nocorrect-pts", "-quiet"}, 
				subprocess::input{subprocess::PIPE}
			);
		} else {
			mplayer_subproc = new subprocess::Popen(
				{"mplayer", "-vo", "matrixview", "-x", "1280", "-idle", "-loop", "10", "-nojoystick", "-nomouseinput", "-slave", fname.c_str()}, 
				subprocess::input{subprocess::PIPE}
			);
		}
	} else {
		fname = string_replace(fname, "%20", "\\ ");  // some file managers replace spaces with %20
		fname = string_replace(fname, "%5B", "\\[");
		fname = string_replace(fname, "%5D", "\\]");
		std::string cmd = "loadfile ";
		cmd += fname;
		cmd += "\n";
		mplayer_subproc->send(cmd.c_str(), cmd.size());
	}
}


using namespace std;

#ifdef _DEBUG
#define DEBUGMSG( ... ) fprintf( stderr, __VA_ARGS__ )
#else
#define DEBUGMSG( ... )
#endif


Configuration & config = Configuration::get();

#ifdef ENABLE_REALTIME
static void sched_realtime()
{
#ifdef linux
	struct sched_param sched = {0};
	sched.sched_priority = 50;
	int foo = sched_setscheduler(0, SCHED_FIFO, &sched);
	sched_getparam(0, &sched);

	if (foo) {
		DEBUGMSG(_("Failed to set SCHED_FIFO\n"));
		config.realtime = 0;
	}
	else {
		DEBUGMSG("Set SCHED_FIFO\n");
		config.realtime = 1;
	}
#elif defined(__APPLE__)
	// CoreAudio apps don't need realtime priority for decent performance
#else
#warning "sched_realtime not implemented for this OS"
#endif
}
#endif

////////////////////////////////////////////////////////////////////////////////

void ptest ();

static MidiDriver *midiDriver;
Synthesizer *s_synthesizer;
static unsigned char *midiBuffer;
static const size_t midiBufferSize = 4096;
static int gui_midi_pipe[2];

class UpdateGamepad { 
	public:
		Synthesizer* editor;
		SDL_Joystick *primary_joystick;
		SDL_Joystick *secondary_joystick;
		SDL_Joystick *third_joystick;
		SDL_Joystick *pad1;
		SDL_Joystick *pad2;
		SDL_Joystick *pad3;
		//SDL_Joystick *fourth_joystick;
		int gamepadHat1 = 0;
		int gamepadHat2 = 0;
		int gamepadHat3 = 0;
		// analog locks
		bool pad1analog1_locked = false;
		bool pad1analog2_locked = false;
		bool pad2analog1_locked = false;
		bool pad2analog2_locked = false;
		bool pad3analog1_locked = false;
		bool pad3analog2_locked = false;
		std::pair<float,float> pad1analog1 = std::pair<float,float>(0.0, 0.0);
		std::pair<float,float> pad1analog2 = std::pair<float,float>(0.0, 0.0);
		std::pair<float,float> pad2analog1 = std::pair<float,float>(0.0, 0.0);
		std::pair<float,float> pad2analog2 = std::pair<float,float>(0.0, 0.0);
		std::pair<float,float> pad3analog1 = std::pair<float,float>(0.0, 0.0);
		std::pair<float,float> pad3analog2 = std::pair<float,float>(0.0, 0.0);
		int prevNote = -1;
		bool analogKeyboard = false;
		int analogKeyboardOffset = 0;
		int gamepad_shift = -1;

		void shift_gamepads(int shift) {
			this->gamepad_shift += shift;
			if (this->gamepad_shift < 0)
				this->gamepad_shift = 0;

			if (this->pad3) {
				if (this->gamepad_shift >= 3)
					this->gamepad_shift = 2;

				switch (this->gamepad_shift) {
					case 0:
						this->primary_joystick = this->pad1;
						this->secondary_joystick = this->pad2;
						this->third_joystick = this->pad3;
						break;
					case 1:
						this->primary_joystick = this->pad3;
						this->secondary_joystick = this->pad1;
						this->third_joystick = this->pad2;
						break;
					case 2:
						this->primary_joystick = this->pad2;
						this->secondary_joystick = this->pad3;
						this->third_joystick = this->pad1;
						break;
				}        
			}
			else {
				if (this->gamepad_shift >= 2)
					this->gamepad_shift = 1;

				switch (this->gamepad_shift) {
					case 0:
						this->primary_joystick = this->pad1;
						this->secondary_joystick = this->pad2;
						break;
					case 1:
						this->primary_joystick = this->pad2;
						this->secondary_joystick = this->pad1;
						break;
				}
			}
		}

		UpdateGamepad( Synthesizer* editor_, SDL_Joystick* p1, SDL_Joystick* p2, SDL_Joystick* p3) {
			this->editor = editor_;
			this->primary_joystick = p1;
			this->secondary_joystick = p2;
			this->third_joystick = p3;
			this->pad1 = p1;
			this->pad2 = p2;
			this->pad3 = p3;
			if (this->primary_joystick) {
				this->editor->linkGamepadAxis(std::string("osc_feedback_amount"), 0);
				this->editor->linkGamepadAxis(std::string("cutoff"), 1);
				this->editor->linkGamepadAxis(std::string("portamento"), 2);
				this->editor->linkGamepadAxis(std::string("resonance"), 3);
			}
			if (this->secondary_joystick) {
				this->analogKeyboard = true;
				this->editor->linkGamepadAxis(std::string("osc_feedback_amount"), 1);
				this->editor->linkGamepadAxis(std::string("cutoff"), 2);
				this->editor->linkGamepadAxis(std::string("portamento"), 3);
				this->editor->linkGamepadAxis(std::string("resonance"), 3);
				this->editor->linkGamepadButton(std::string("formant_on"), 4);
				this->editor->linkGamepadButton(std::string("delay_on"), 4);
				this->editor->linkGamepadButton(std::string("filter_on"), 5);
				this->editor->linkGamepadButton(std::string("stutter_on"), 5);
				this->editor->linkGamepadButton(std::string("distortion_on"), 6);
				this->editor->linkGamepadButton(std::string("reverb_on"), 7);
				this->editor->linkGamepadButton(std::string("arp_on"), 7);
				this->editor->linkGamepadAxis(std::string("arp_gate"), 4);
				this->editor->linkGamepadAxis(std::string("sub_shuffle"), 4);
				this->editor->linkGamepadAxis(std::string("beats_per_minute"), 5);
				this->editor->linkGamepadAxis(std::string("formant_x"), 6);
				this->editor->linkGamepadAxis(std::string("formant_y"), 7);
			}
			if (this->third_joystick) {
				this->editor->linkGamepadAxis(std::string("osc_1_tune"), 8);
				this->editor->linkGamepadAxis(std::string("delay_feedback"), 9);

				this->editor->linkGamepadAxis(std::string("osc_2_tune"), 10);
				this->editor->linkGamepadAxis(std::string("cross_modulation"), 11);
			}
		}
		~UpdateGamepad() {
			if (this->primary_joystick)
				SDL_JoystickClose(this->primary_joystick);
			if (this->secondary_joystick)
				SDL_JoystickClose(this->secondary_joystick);
			if (this->third_joystick)
				SDL_JoystickClose(this->third_joystick);
		}
		void timerCallback() {
			// secondary gamepad
			float x3 = 0.0;
			float y3 = 0.0;
			float x4 = 0.0;
			float y4 = 0.0;
			// 3rd gamepad
			float x5 = 0.0;
			float y5 = 0.0;
			float x6 = 0.0;
			float y6 = 0.0;

			int btns[12]  = {0,0,0,0, 0,0,0,0, 0,0,0,0};
			int btns2[12] = {0,0,0,0, 0,0,0,0, 0,0,0,0};
			int btns3[12] = {0,0,0,0, 0,0,0,0, 0,0,0,0};

			SDL_PumpEvents();  // poll event not required when calling pump
			int hat = SDL_JoystickGetHat(this->primary_joystick, 0);
			bool button_lock = hat == 1;  // if hat is up
			if (hat != this->gamepadHat1) {
				//std::cout << hat << std::endl;
				switch (hat) {
					case 1: // up
						this->analogKeyboard=true;
						break;
					case 2: // right
						this->editor->nextPatch();
						break;
					case 4: // down
						this->analogKeyboard=false;
						break;
					case 8: // left
						this->editor->prevPatch();
						break;
				}
				//if (hat==0)
				//  emit gamepadHatReleased();
				//else
				//  emit gamepadHatPressed(hat);
			}
			this->gamepadHat1 = hat;
			float x1 = ((double)SDL_JoystickGetAxis(this->primary_joystick, 0)) / 32768.0;
			float y1 = ((double)SDL_JoystickGetAxis(this->primary_joystick, 1)) / 32768.0;
			//double z1 = (((double)SDL_JoystickGetAxis(m_joystick, 2)) / 32768.0) + 1.0;  // xbox gamepads have 6 axes
			float x2 = ((double)SDL_JoystickGetAxis(this->primary_joystick, 2)) / 32768.0;
			float y2 = ((double)SDL_JoystickGetAxis(this->primary_joystick, 3)) / 32768.0;
			//double z2 = (((double)SDL_JoystickGetAxis(m_joystick, 5)) / 32768.0) + 1.0;

			for (int i=0; i<12; i++) {
				btns[i] = 0;
				if (SDL_JoystickGetButton(this->primary_joystick, i)) {
					if (gamepad1Buttons[i]==0) {
						gamepad1Buttons[i] = 1;
						btns[i] = 1;
						if (use_pipe)
							std::cout << "{\"button\":" << i << ",\"value\":1}" << std::endl;
					} else {
						gamepad1Buttons[i] += 1;
						btns[i] = gamepad1Buttons[i];
					}
				} else {
					if (gamepad1Buttons[i] >= 1) {
						gamepad1Buttons[i] = 0;
						btns[i] = -1;
						if (use_pipe)
							std::cout << "{\"button\":" << i << ",\"value\":-1}" << std::endl;
					}
				}
			}

			if (btns[8]==1)  // select button
				this->pad1analog1_locked = false;
			else if (btns[10] == 1){  // left analog button
				this->pad1analog1_locked = true;
				this->pad1analog1.first = x1;
				this->pad1analog1.second = y1;
			}
			if (btns[9]==1)  // start button
				this->pad1analog2_locked = false;
			else if (btns[11] == 1){  // right analog button
				this->pad1analog2_locked = true;
				this->pad1analog2.first = x2;
				this->pad1analog2.second = y2;
			}
			if (this->pad1analog1_locked){
				x1 = this->pad1analog1.first;
				y1 = this->pad1analog2.second;
			}
			if (this->pad1analog2_locked){
				x2 = this->pad1analog2.first;
				y2 = this->pad1analog2.second;
			}

			if (this->secondary_joystick) {
				x3 = ((double)SDL_JoystickGetAxis(this->secondary_joystick, 0)) / 32768.0;
				y3 = ((double)SDL_JoystickGetAxis(this->secondary_joystick, 1)) / 32768.0;
				x4 = ((double)SDL_JoystickGetAxis(this->secondary_joystick, 2)) / 32768.0;
				y4 = ((double)SDL_JoystickGetAxis(this->secondary_joystick, 3)) / 32768.0;

				for (int i=0; i<12; i++) {
					btns2[i] = 0;
					if (SDL_JoystickGetButton(this->secondary_joystick, i)) {
						if (gamepad2Buttons[i]==0) {
							gamepad2Buttons[i] = 1;
							btns2[i] = 1;
						} else {
							gamepad2Buttons[i] += 1;
							btns2[i] = gamepad2Buttons[i];
						}
					} else {
						if (gamepad2Buttons[i] >= 1) {
							gamepad2Buttons[i] = 0;
							btns2[i] = -1;
						}
					}
				}

				if (btns2[8]==1)  // select button
					this->pad2analog1_locked = false;
				else if (btns2[10] == 1){  // left analog button
					this->pad2analog1_locked = true;
					this->pad2analog1.first = x3;
					this->pad2analog1.second = y3;
				}
				if (btns2[9]==1)  // start button
					this->pad2analog2_locked = false;
				else if (btns2[11] == 1){  // right analog button
					this->pad2analog2_locked = true;
					this->pad2analog2.first = x4;
					this->pad2analog2.second = y4;
				}
				if (this->pad2analog1_locked){
					x3 = this->pad2analog1.first;
					y3 = this->pad2analog2.second;
				}
				if (this->pad2analog2_locked){
					x4 = this->pad2analog2.first;
					y4 = this->pad2analog2.second;
				}

				int hat2 = SDL_JoystickGetHat(this->secondary_joystick, 0);
				if (hat2 != this->gamepadHat2) {
					switch (hat2) {
						case 1: // up
							this->shift_gamepads(-1);
							break;
						case 4: // down
							this->shift_gamepads(1);
							break;
						case 2: // right
							this->editor->nextPatch();
							break;
						case 8: // left
							this->editor->prevPatch();
							break;
					}
				}
				this->gamepadHat2 = hat2;
			}


			// mplayer gamepad control //
			if (mplayer_subproc) {
				//std::string cmd = "speed_mult ";  // not as responsive as speed_set
				std::string cmd = "speed_set ";
				cmd += std::to_string(
					(x1+1) + x2 + (y1*0.25) + (y2*0.25) +
					((x3+1)*0.2) + (x4*0.4) + (y3*0.1) + (y4*0.1)
				);
				cmd += "\n";  // required to send a new line at the end of the command
				mplayer_subproc->send(cmd.c_str(), cmd.size());
				if (x1 > 0.8) {
					std::uniform_real_distribution<double> unifx(-x1*1000, x1*1000);
					double xx = unifx(*__rand_engine);
					std::uniform_real_distribution<double> unify(-y1*10, y1*10);
					double yy = unify(*__rand_engine);
					amsynth_glitch_background_redraw( int(xx), int(yy), true, __background_text__ );
				} else if (mplayer_webcam_ascii_subproc) {
					amsynth_glitch_background_redraw( 0,0,false, __background_text__ );
				}

			} else if (mplayer_webcam_ascii_subproc) {
				amsynth_glitch_background_redraw( 0,0,true, __background_text__ );
			}
			/*  these will not work :(
			if (mplayer_webcam_subproc){
				std::string cmd2 = "contrast ";
				cmd2 += std::to_string( (int)((x2+1)*100) );
				cmd2 += "\n";
				mplayer_webcam_subproc->send(cmd2.c_str(), cmd2.size());				
			}
			if (mplayer_webcam_ascii_subproc){
				std::string cmd2 = "tv_set_contrast ";
				cmd2 += std::to_string( (int)((x2+1)*100) );
				cmd2 += "\n";
				mplayer_webcam_ascii_subproc->send(cmd2.c_str(), cmd2.size());				
			}
			*/

			if (this->third_joystick) {
				x5 = ((double)SDL_JoystickGetAxis(this->third_joystick, 0)) / 32768.0;
				y5 = ((double)SDL_JoystickGetAxis(this->third_joystick, 1)) / 32768.0;
				x6 = ((double)SDL_JoystickGetAxis(this->third_joystick, 2)) / 32768.0;
				y6 = ((double)SDL_JoystickGetAxis(this->third_joystick, 3)) / 32768.0;

				for (int i=0; i<12; i++) {
					btns3[i] = 0;
					if (SDL_JoystickGetButton(this->third_joystick, i)) {
						if (gamepad3Buttons[i]==0) {
							gamepad3Buttons[i] = 1;
							btns3[i] = 1;
						} else {
							gamepad3Buttons[i] += 1;
							btns3[i] = gamepad3Buttons[i];
						}
					} else {
						if (gamepad3Buttons[i] >= 1) {
							gamepad3Buttons[i] = 0;
							btns3[i] = -1;
						}
					}
				}

				if (btns3[8]==1)  // select button
					this->pad3analog1_locked = false;
				else if (btns3[10] == 1){  // left analog button
					this->pad3analog1_locked = true;
					this->pad3analog1.first = x5;
					this->pad3analog1.second = y5;
				}
				if (btns3[9]==1)  // start button
					this->pad3analog2_locked = false;
				else if (btns2[11] == 1){  // right analog button
					this->pad3analog2_locked = true;
					this->pad3analog2.first = x6;
					this->pad3analog2.second = y6;
				}
				if (this->pad3analog1_locked){
					x5 = this->pad3analog1.first;
					y5 = this->pad3analog2.second;
				}
				if (this->pad3analog2_locked){
					x6 = this->pad3analog2.first;
					y6 = this->pad3analog2.second;
				}

				int hat3 = SDL_JoystickGetHat(this->third_joystick, 0);
				if (hat3 != this->gamepadHat3) {
					switch (hat3) {
						case 1: // up
							this->shift_gamepads(-1);
							break;
						case 4: // down
							this->shift_gamepads(1);
							break;
						case 2: // right
							this->editor->nextPatch();
							break;
						case 8: // left
							this->editor->prevPatch();
							break;
					}
				}
				this->gamepadHat3 = hat3;
			}


			if ( btns[0]==1 || btns2[0]==1 || btns3[0]==1 ){
			}
			if ( btns[1] == 1 || btns2[1]==1 || btns3[1]==1 ){
			}

			if (btns[2]==1  || btns2[2]==1 || btns3[2]==1 ){
			}
			//else if (btns[2] >= 1){
			//  this->playback_rate = x1 + x2 + 1.0;
			//  if (this->playback_rate < 0.01)
			//    this->playback_rate = 0.01;
			//}
			if (btns[3]==1){
				std::cout << "toggle analog keyboard" << std::endl;
				this->editor->noteOff( this->prevNote );
				this->analogKeyboard = ! this->analogKeyboard;
			}


			if (this->analogKeyboard) {
				float x2mult = 8.0;

				if (btns[4] >= 1){
					this->analogKeyboardOffset = static_cast<int>( (x2+0.5) * 40);
				}
				else if (btns[5] >= 1) {
							x2mult *= 0.3;
				}
				if (btns[6] >= 1)
							x2mult *= 2;
				if (btns[7] >= 1)
							x2mult *= 3;

				/*
				else if (btns[8]==1)
							this->analogKeyboardOffset -= 1;
				else if (btns[9]==1)
							this->analogKeyboardOffset += 1;
				*/

				//int keyindex = static_cast<int>( (x1 + x2) * 64.0 );
				//int keyindex = static_cast<int>( ((x1+0.5)*64.0) + (x2*32.0) );
				int keyindex = static_cast<int>( ((x1+1.0)*64.0) + (x2*x2mult) );
				keyindex += this->analogKeyboardOffset;
				if (keyindex != this->prevNote) {
					if (this->prevNote != -1)
						this->editor->noteOff( this->prevNote );
					this->editor->noteOn( keyindex, -y2+1.1 );
					this->prevNote = keyindex;
				}
			}

			if (use_pipe) {
				std::cout << "{"
				<< "\"pad1\":["
					<< x1 << "," << y1 << "," << x2 << "," << y2 << "]" << ","
				<< "\"pad2\":["
					<< x3 << "," << y3 << "," << x4 << "," << y4 << "]" << ","
				<< "\"pad3\":["
					<< x5 << "," << y5 << "," << x6 << "," << y6 << "]"
				<< "}" << std::endl;
			}

			// updates slider buttons set by user from UI
			this->editor->updateGamepad(
				x1,y1, x2,y2, // primary gamepad
				x3,y3, x4,y4, // optional secondary gamepad
				x5,y5, x6,y6, // optional 3rd gamepad
				btns[0], 
				btns[1], 
				btns[2], 
				btns[3], 
				btns[4], 
				btns[5], 
				btns[6], 
				btns[7], 
				btns[8], 
				btns[9], 
				btns[10], 
				btns[11],
				button_lock
			);

			if (mplayer_webcam_ascii_subproc) {
				for (unsigned int i=0; i<ascii_audio_mapping.size(); i++ ) {
					auto p = ascii_audio_mapping[i];
					float val = (float)mplayer_ascii_buf[ p.second ] * (1.0/128.0);
					//std::cout << p.first << val << std::endl;
					this->editor->valueChangedThroughMidi(p.first, val );
				}
			}

		}
};

static UpdateGamepad *s_gamepad_manager = NULL;
unsigned gamepad_timer_callback() {
	s_gamepad_manager->timerCallback();
	return 1;
}


////////////////////////////////////////////////////////////////////////////////

static GenericOutput * open_audio()
{	
#if	__APPLE__

	if (config.audio_driver == "jack" ||
		config.audio_driver == "JACK" ){
		JackOutput *jack = new JackOutput();
		if (jack->init() != 0) {
			delete jack;
			return NULL;
		}
		return jack;
	}

	if (config.audio_driver == "coreaudio" ||
		config.audio_driver == "auto")
		return CreateCoreAudioOutput();

	return NULL;
	
#else

	if (config.audio_driver == "jack" ||
		config.audio_driver == "auto")
	{
		JackOutput *jack = new JackOutput();
		if (jack->init() == 0)
		{
			return jack;
		}
		else
		{
			std::string jack_error = jack->get_error_msg();
			delete jack;
			
			// we were asked specifically for jack, so don't use anything else
			if (config.audio_driver == "jack") {
				std::cerr << _("JACK init failed: ") << jack_error << "\n";
				return new NullAudioOutput;
			}
		}
	}
	
	return new AudioOutput();
	
#endif
}

static MidiDriver *opened_midi_driver(MidiDriver *driver)
{
	if (driver && driver->open() != 0) {
		delete driver;
		return NULL;
	}
	return driver;
}

static void open_midi()
{
	const char *alsa_client_name = "amsynth";
	
	if (config.midi_driver == "alsa" || config.midi_driver == "ALSA") {
		if (!(midiDriver = opened_midi_driver(CreateAlsaMidiDriver(alsa_client_name)))) {
			std::cerr << _("error: could not open ALSA MIDI interface") << endl;
			std::cerr << "falling back to OSS MIDI interface" << endl;
			if (!(midiDriver = opened_midi_driver(CreateOSSMidiDriver()))) {
				std::cerr << _("error: could not open OSS MIDI interface") << endl;
			}
		}
		return;
	}

	if (config.midi_driver == "oss" || config.midi_driver == "OSS") {
		if (!(midiDriver = opened_midi_driver(CreateOSSMidiDriver()))) {
			std::cerr << _("error: could not open OSS MIDI interface") << endl;
		}
		return;
	}

	if (config.midi_driver == "auto") {
		midiDriver = opened_midi_driver(CreateAlsaMidiDriver(alsa_client_name)) ?:
								 opened_midi_driver(CreateOSSMidiDriver());
		if (config.current_midi_driver.empty()) {
			std::cerr << _("error: could not open any MIDI interface") << endl;
		}
		return;
	}
}

static void fatal_error(const std::string & msg)
{
	std::cerr << msg << "\n";
#ifdef WITH_GUI
	ShowModalErrorMessage(msg);
#endif
	exit(1);
}

unsigned amsynth_timer_callback();

static int signal_received = 0;

static void signal_handler(int signal)
{
	signal_received = signal;
}

#ifdef EMBED_AMSYNTH
extern "C" int amsynth_main( int argc, char *argv[] ) {
#else
int main( int argc, char *argv[] ) {
#endif
	std::random_device rand_dev;
	__rand_engine = new std::mt19937(rand_dev());
	srand((unsigned) time(NULL));

#ifdef ENABLE_REALTIME
	sched_realtime();

	// need to drop our suid-root permissions :-
	// GTK will not work SUID for security reasons..
	setreuid(getuid(), getuid());
	setregid(getgid(), getgid());
#endif

#if ENABLE_NLS
	setlocale(LC_ALL, "");
	bindtextdomain(GETTEXT_PACKAGE, PACKAGE_LOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);
#else
#warning text will not be localized because ENABLE_NLS not set
#endif

	int initial_preset_no = 0;

	// needs to be called before our own command line parsing code
	amsynth_lash_process_args(&argc, &argv);
	
	bool no_gui = (getenv("AMSYNTH_NO_GUI") != NULL);
	auto dtype = getenv("DESKTOP_SESSION");
	if (dtype != NULL)
		desktop_type = std::string(dtype);
	else {
		dtype = getenv("XDG_SESSION_DESKTOP");
		if (dtype != NULL)
			desktop_type = std::string(dtype);
	}

	static struct option longopts[] = {
		{ "jack_autoconnect", optional_argument, NULL, 0 },
		{ 0 }
	};
	
	int opt, longindex = -1;
	while ((opt = getopt_long(argc, argv, "vhsdzxm:c:a:r:p:b:U:P:n:t:", longopts, &longindex)) != -1) {
		switch (opt) {
						case 'v':
								cout << "amsynth-raptor-hack" << endl;
				return 0;
			case 'h':
				cout << _("usage: ") << "amsynth" << _(" [options]") << endl
						 << endl
						 << _("Any options given here override those in the config file ($HOME/.amSynthrc)") << endl
						 << endl
						 << _("OPTIONS:") << endl
						 << endl
						 << _("	-h          show this usage message") << endl
						 << _("	-v          show version information") << endl
						 << _("	-x          run in headless mode (without GUI)") << endl
						 << endl
						 << _("	-b <file>   use <file> as the bank to store presets") << endl
						 << _("	-t <file>   use <file> as a tuning file") << endl
						 << endl
						 << _("	-a <string> set the sound output driver to use [alsa/oss/auto(default)]") << endl
						 << _("	-r <int>    set the sampling rate to use") << endl
						 << _("	-m <string> set the MIDI driver to use [alsa/oss/auto(default)]") << endl
						 << _("	-c <int>    set the MIDI channel to respond to (default=all)") << endl
						 << _("	-p <int>    set the polyphony (maximum active voices)") << endl
						 << endl
						 << _("	-n <name>   specify the JACK client name to use") << endl
						 << _("	--jack_autoconnect[=<true|false>]") << endl
						 << _("	            automatically connect jack audio ports to hardware I/O ports. (Default: true)") << endl
						 << endl;

				return 0;
			case 'z':
				ptest();
				return 0;
			case 'P':
				initial_preset_no = atoi(optarg);
				break;
			case 'x':
				no_gui = true;
				break;
			case 'm':
				config.midi_driver = optarg;
				break;
			case 'b':
				config.current_bank_file = optarg;
				break;
			case 'c':
				config.midi_channel = atoi( optarg );
				break;
			case 'a':
				config.audio_driver = optarg;
				break;
			case 'r':
				config.sample_rate = atoi( optarg );
				break;
			case 'p':
				config.polyphony = atoi( optarg );
				break;
			case 't':
				config.current_tuning_file = optarg;
				break;
			case 'U':
				config.jack_session_uuid = optarg;
				break;
			case 'n':
				config.jack_client_name_preference = optarg;
				break;
			case 0:
				if (strcmp(longopts[longindex].name, "jack_autoconnect") == 0) {
					JackOutput::autoconnect = !optarg || (strcmp(optarg, "true") == 0);
				}
				break;
			default:
				break;
		}
	}

#ifdef WITH_GUI
	if (!no_gui)
		gui_kit_init(&argc, &argv);
#endif

	string amsynth_bank_file = config.current_bank_file;
	// string amsynth_tuning_file = config.current_tuning_file;

	GenericOutput *out = open_audio();
	if (!out)
		fatal_error(std::string(_("Fatal Error: open_audio() returned NULL.\n")) +
					"config.audio_driver = " + config.audio_driver);

	// errors now detected & reported in the GUI
	out->init();

	s_synthesizer = new Synthesizer();
	s_synthesizer->setSampleRate(config.sample_rate);
	
	amsynth_load_bank(config.current_bank_file.c_str());
	amsynth_set_preset_number(initial_preset_no);

#ifdef WITH_NSM
	NsmClient nsmClient(argv[0]);
	NsmHandler nsmHandler(&nsmClient);
	nsmClient.Init(PACKAGE_NAME);
#endif

	if (config.current_tuning_file != "default")
		amsynth_load_tuning_file(config.current_tuning_file.c_str());
	
	// errors now detected & reported in the GUI
	out->Start();
	
	open_midi();
	midiBuffer = (unsigned char *)malloc(midiBufferSize);

	// prevent lash from spawning a new jack server
	setenv("JACK_NO_START_SERVER", "1", 0);
	
	if (config.alsa_seq_client_id != 0 || !config.jack_client_name.empty())
		// LASH only works with ALSA MIDI and JACK
		amsynth_lash_init();

	if (config.alsa_seq_client_id != 0) // alsa midi is active
		amsynth_lash_set_alsa_client_id((unsigned char) config.alsa_seq_client_id);

	if (!config.jack_client_name.empty())
		amsynth_lash_set_jack_client_name(config.jack_client_name.c_str());

	// give audio/midi threads time to start up first..
	// if (jack) sleep (1);

	if (pipe(gui_midi_pipe) != -1) {
		fcntl(gui_midi_pipe[0], F_SETFL, O_NONBLOCK);
	}



	//SDL_Init(SDL_INIT_JOYSTICK);
	SDL_SetHint(SDL_HINT_JOYSTICK_ALLOW_BACKGROUND_EVENTS,"1");  // required in SDL2 when initialized before display
	SDL_InitSubSystem(SDL_INIT_JOYSTICK);
	// Check for joystick
	bool has_gamepads = false;
	//unsigned int tid = 0;
	if (SDL_NumJoysticks() > 0) {
		has_gamepads = true;
		// Open joystick
		auto joystick1 = SDL_JoystickOpen(gamepad_offset);
		auto joystick2 = SDL_JoystickOpen(gamepad_offset + 1);
		auto joystick3 = SDL_JoystickOpen(gamepad_offset + 2);
		if (joystick1) {
			printf("Primary Gamepad %i\n", gamepad_offset);
			printf("Primary Name: %s\n", SDL_JoystickNameForIndex(gamepad_offset));  // SDL2
			printf("Number of Axes: %d\n", SDL_JoystickNumAxes(joystick1));
			printf("Number of Buttons: %d\n", SDL_JoystickNumButtons(joystick1));
			printf("Number of Balls: %d\n", SDL_JoystickNumBalls(joystick1));
			printf("Number of Hats: %d\n", SDL_JoystickNumHats(joystick1));
			if (joystick2){
				printf("Secondary Gamepad %i\n", gamepad_offset+1);
				printf("Secondary Name: %s\n", SDL_JoystickNameForIndex(gamepad_offset + 1));
			}
			if (joystick3){
				printf("3rd Gamepad %i\n", gamepad_offset+2);
				printf("3rd Name: %s\n", SDL_JoystickNameForIndex(gamepad_offset + 2));
			}
			s_gamepad_manager = new UpdateGamepad(s_synthesizer, joystick1, joystick2, joystick3);

		} else {
			printf("Couldn't open gamepad %i\n", gamepad_offset);
		}
	} else {
		printf("No gamepads are attached\n"); 
	}

#ifdef WITH_GUI
	if (!no_gui) {
		if (has_gamepads) {
			//tid = g_timeout_add(50, (GSourceFunc)gamepad_timer_callback, NULL);
			g_timeout_add(50, (GSourceFunc)gamepad_timer_callback, NULL);
		}
		AmsynthXID = main_window_show(s_synthesizer, out);
		std::cout << "xid = " << AmsynthXID << std::endl;
		gui_kit_run(&amsynth_timer_callback);
	} else {
#endif
		printf(_("amsynth running in headless mode, press ctrl-c to exit\n"));
		signal(SIGINT, &signal_handler);
		while (!signal_received)
			sleep(2); // delivery of a signal will wake us early
		printf("\n");
		printf(_("shutting down...\n"));
#ifdef WITH_GUI
	}
#endif

	out->Stop();
	if (config.xruns) std::cerr << config.xruns << _(" audio buffer underruns occurred\n");
	delete out;

	bool ensure_mplayer_dead = false;
	std::string cmd = "exit\n";
	if (mplayer_subproc) {
		ensure_mplayer_dead = true;
		mplayer_subproc->send(cmd.c_str(), cmd.size());
		delete mplayer_subproc;
	}
	if (mplayer_webcam_subproc){
		ensure_mplayer_dead = true;
		mplayer_webcam_subproc->send(cmd.c_str(), cmd.size());
		delete mplayer_webcam_subproc;
	}
	if (mplayer_webcam_ascii_subproc){
		ensure_mplayer_dead = true;
		mplayer_webcam_ascii_subproc->send(cmd.c_str(), cmd.size());
		delete mplayer_webcam_ascii_subproc;
		//ascii_audio_thread.join();
	}
	if (ensure_mplayer_dead) {
		system("killall mplayer");
	}
	if (has_gamepads) {
		// https://developer.gnome.org/gtk2/stable/gtk2-General.html#gtk-timeout-remove
		// deprecated, missing in ubuntu19
		//gtk_timeout_remove(tid);
		//delete s_gamepad_manager;
		//SDL_Quit();
	}
	std::cout << "amsynth exit" << std::endl;
	//https://stackoverflow.com/questions/17682934/linux-terminal-typing-feedback-gone-line-breaks-not-displayed
	system("\x0Areset\x0A");
	return 0;
}

unsigned amsynth_timer_callback() {
	amsynth_lash_poll_events();
	return 1;
}


void amsynth_midi_input(unsigned char status, unsigned char data1, unsigned char data2)
{
	unsigned char buffer[3] = { status, data1, data2 };
	if (config.midi_channel > 1) {
		buffer[0] |= ((config.midi_channel - 1) & 0x0f);
	}
	write(gui_midi_pipe[1], buffer, sizeof(buffer));
}

static bool compare(const amsynth_midi_event_t &first, const amsynth_midi_event_t &second) {
	return (first.offset_frames < second.offset_frames);
}

void amsynth_audio_callback(
		float *buffer_l, float *buffer_r, unsigned num_frames, int stride,
		const std::vector<amsynth_midi_event_t> &midi_in,
		std::vector<amsynth_midi_cc_t> &midi_out)
{
	std::vector<amsynth_midi_event_t> midi_in_merged = midi_in;

	if (midiBuffer) {
		unsigned char *buffer = midiBuffer;
		ssize_t bufferSize = midiBufferSize;
		memset(buffer, 0, bufferSize);

		if (gui_midi_pipe[0]) {
			ssize_t bytes_read = read(gui_midi_pipe[0], buffer, bufferSize);
			if (bytes_read > 0) {
				amsynth_midi_event_t event = {0};
				event.offset_frames = num_frames - 1;
				event.length = (unsigned int) bytes_read;
				event.buffer = buffer;
				midi_in_merged.push_back(event);
				buffer += bytes_read;
				bufferSize -= bytes_read;
			}
		}

		if (midiDriver) {
			int bytes_read = midiDriver->read(buffer, (unsigned) bufferSize);
			if (bytes_read > 0) {
				amsynth_midi_event_t event = {0};
				event.offset_frames = num_frames - 1;
				event.length = bytes_read;
				event.buffer = buffer;
				midi_in_merged.push_back(event);
			}
		}
	}

	std::sort(midi_in_merged.begin(), midi_in_merged.end(), compare);

	if (s_synthesizer) {
		s_synthesizer->process(num_frames, midi_in_merged, midi_out, buffer_l, buffer_r, stride);
	}

	if (midiDriver && !midi_out.empty()) {
		std::vector<amsynth_midi_cc_t>::const_iterator out_it;
		for (out_it = midi_out.begin(); out_it != midi_out.end(); ++out_it) {
			midiDriver->write_cc(out_it->channel, out_it->cc, out_it->value);
		}
	}
}

void
amsynth_save_bank(const char *filename)
{
		s_synthesizer->saveBank(filename);
}

void
amsynth_load_bank(const char *filename)
{
	s_synthesizer->loadBank(filename);
}

int
amsynth_load_tuning_file(const char *filename)
{
	int result = s_synthesizer->loadTuningScale(filename);
	if (result != 0) {
		cerr << _("error: could not load tuning file ") << filename << endl;
	}
	return result;
}

int
amsynth_get_preset_number()
{
	return s_synthesizer->getPresetNumber();
}

void
amsynth_set_preset_number(int preset_no)
{
	s_synthesizer->setPresetNumber(preset_no);
}

///////////////////////////////////////////////////////////////////////////////

void ptest ()
{
	//
	// test parameters
	// 
	const int kTestBufSize = 64;
	const int kTestSampleRate = 44100;
	const int kTimeSeconds = 60;
	const int kNumVoices = 10;

	float *buffer = new float [kTestBufSize];

	VoiceAllocationUnit *voiceAllocationUnit = new VoiceAllocationUnit;
	voiceAllocationUnit->SetSampleRate (kTestSampleRate);
	
	// trigger off some notes for amsynth to render.
	for (int v=0; v<kNumVoices; v++) {
		voiceAllocationUnit->HandleMidiNoteOn(60 + v, 1.0f);
	}
	
	struct rusage usage_before; 
	getrusage (RUSAGE_SELF, &usage_before);
	
	long total_samples = kTestSampleRate * kTimeSeconds;
	long total_calls = total_samples / kTestBufSize;
	unsigned remain_samples = total_samples % kTestBufSize;
	for (int i=0; i<total_calls; i++) {
		voiceAllocationUnit->Process (buffer, buffer, kTestBufSize);
	}
	voiceAllocationUnit->Process (buffer, buffer, remain_samples);

	struct rusage usage_after; 
	getrusage (RUSAGE_SELF, &usage_after);
	
	unsigned long user_usec = (usage_after.ru_utime.tv_sec*1000000 + usage_after.ru_utime.tv_usec)
							- (usage_before.ru_utime.tv_sec*1000000 + usage_before.ru_utime.tv_usec);
	
	unsigned long syst_usec = (usage_after.ru_stime.tv_sec*1000000 + usage_after.ru_stime.tv_usec)
							- (usage_before.ru_stime.tv_sec*1000000 + usage_before.ru_stime.tv_usec);

	unsigned long usec_audio = kTimeSeconds * kNumVoices * 1000000;
	unsigned long usec_cpu = user_usec + syst_usec;
	
	fprintf (stderr, _("user time: %f		system time: %f\n"), user_usec/1000000.f, syst_usec/1000000.f);
	fprintf (stderr, _("performance index: %f\n"), (float) usec_audio / (float) usec_cpu);
	
	delete [] buffer;
	delete voiceAllocationUnit;
}

